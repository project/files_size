Files size module provides the counts of the file size and show the icon of the file type. 

File size module is depends on the views module. User needs to call show the files by using view and needs to select "URL to File".

There are the configuration option to allow files icon and Mime Type with extension. Please go to the below url after login to the admin.

admin/config/development/files_size (Configuration >> Development >> File icon and Filesize settings).

Example : 
pdf=>application/pdf=>pdf.png
zip=>application/x-compressed,application/x-zip-compressed,application/zip,multipart/x-zip=>zip.png

Here pdf and zip provides the file extension
application/x-compressed and application/x-zip-compressed are the mime type. Here "," is using for adding multiple mime type using for the same extension file.
zip.png and pdf.png is the icon of the file. The path of the file is module path/icons

And => is added for separator and for new row added you needs to add new line and format will be like below.

file extension=>file mime type,file mime type 1,file mime type 2=>file name of the icon.